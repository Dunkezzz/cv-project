import React from 'react';
import '../assets/styles/Panel.scss'; 


const PhotoBox = ({ profileImage, profileName }) => {
  return (
    <div className="photo-box">
      <img src={profileImage} className="user-icon" alt="User-icon" />
      <span className="profile-name">{profileName}</span>
    </div>
  );
};

export default PhotoBox;

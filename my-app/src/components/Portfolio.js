import React, { useState } from 'react';
import '../assets/styles/Portfolio.scss';
import uiCardImage from '../assets/images/uiCard.png';
import codeCardImage from '../assets/images/codeCard.png';

const Portfolio = () => {
  const [filter, setFilter] = useState('All');

  const projects = [
    {
      title: 'UI Project 1',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      category: 'UI',
      image: uiCardImage,
    },
    {
      title: 'Code Project 1',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      category: 'Code',
      image: codeCardImage,
    },
    {
      title: 'UI Project 2',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      category: 'UI',
      image: uiCardImage,
    },
    {
      title: 'Code Project 2',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      category: 'Code',
      image: codeCardImage,
    },
  ];

  const handleFilterChange = (category) => {
    setFilter(category);
  };

  const filteredProjects = filter === 'All' ? projects : projects.filter((project) => project.category === filter);

  return (
    <div className="portfolio">
      <div className="filter-tabs">
        <button className={filter === 'All' ? 'active' : ''} onClick={() => handleFilterChange('All')}>
          All
        </button>
        <button className={filter === 'UI' ? 'active' : ''} onClick={() => handleFilterChange('UI')}>
          UI
        </button>
        <button className={filter === 'Code' ? 'active' : ''} onClick={() => handleFilterChange('Code')}>
          Code
        </button>
      </div>
      <div className={`project-cards ${filter.toLowerCase()}`}>
        {filteredProjects.map((project, index) => (
          <div className="project-card" key={index}>
            <img src={project.image} alt={project.title} />
            <div className="project-info">
              <h3>{project.title}</h3>
              <p>{project.description}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Portfolio;

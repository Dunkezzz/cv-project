import React from 'react';
import '../assets/styles/Address.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faTwitter, faFacebookF, faSkype } from '@fortawesome/free-brands-svg-icons';

const Address = () => {
  const openEmailClient = () => {
    window.location.href = 'mailto:office@kamsolutions.pl';
  };

  return (
    <div className="address">
      <div className="contact">
        <FontAwesomeIcon icon={faPhone} className="contact-icon" />
        <span>500 342 242</span>
      </div>
      <div className="contact">
        <FontAwesomeIcon icon={faEnvelope} className="contact-icon" />
        <span onClick={openEmailClient}>office@kamsolutions.pl</span>
      </div>
      <div className="contact">
        <FontAwesomeIcon icon={faTwitter} className="contact-icon" />
        <span>
          Twitter<span className="direct-link">twitter.com</span>
        </span>
      </div>
      <div className="contact">
        <FontAwesomeIcon icon={faFacebookF} className="contact-icon" />
        <span>
          Facebook<span className="direct-link">facebook.com</span>
        </span>
      </div>
      <div className="contact">
        <FontAwesomeIcon icon={faSkype} className="contact-icon" />
        <span>
          Skype<span className="direct-link">skype.com</span>
        </span>
      </div>
    </div>
  );
};

export default Address;

import React, { useState } from 'react';
import '../assets/styles/Panel.scss';
import profileImage from '../assets/images/profile.jpg';
import PhotoBox from './PhotoBox';
import Navigation from './Navigation';
import BackButton from './GoBackButton';

const Panel = () => {
  const [isPanelOpen, setIsPanelOpen] = useState(true);

  const togglePanel = () => {
    setIsPanelOpen(!isPanelOpen);
  };

  return (
    <div className={`panel ${isPanelOpen ? '' : 'closed'}`}>
      <nav className="navbar">
        <div className={`nav-content ${isPanelOpen ? '' : 'hidden'}`}>
          {isPanelOpen && (
            <>
              <PhotoBox profileImage={profileImage} profileName="Yerzhan Mukushev" className="photo-box" />
              <Navigation />
              <BackButton />
            </>
          )}
        </div>
      </nav>

      <div className={`burger-button ${isPanelOpen ? '' : 'closed'}`} onClick={togglePanel}>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  );
};

export default Panel;










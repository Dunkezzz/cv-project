import React from 'react';
import '../assets/styles/Feedback.scss';


const Feedback = ({ data }) => {
    return (
      <section className='feedback-section'>
        {data.map((item, index) => (
          <div key={index} className='feedback-card'>
            <p>{item.feedback}</p>
            <div className='info'>
              <img className='feedbacker-image' src={item.reporter.photoUrl} alt={item.reporter.name} />
              <name className='name'>{item.reporter.name}</name>
              <a className='link'href={item.reporter.citeUrl}>www.citeexample.com</a>
            </div>
          </div>
        ))}
      </section>
    );
  };
  
  export default Feedback;
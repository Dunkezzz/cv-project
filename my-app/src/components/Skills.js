import React, { useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import '../assets/styles/Skills.scss';

const Skills = () => {
  const skillsData = [
    { skill: 'HTML', level: 'Expert' },
    { skill: 'CSS', level: 'Proficient' },
    { skill: 'jQuery', level: 'Proficient' },
    { skill: 'PHP', level: 'Beginner' },
    { skill: 'Laravel', level: 'Proficient' }
  ];

  const [isEditOpen, setIsEditOpen] = useState(false); // State to manage form visibility
  const [newSkills, setNewSkills] = useState([]); // State to store newly added skills

  const handleSubmit = (values, { resetForm }) => {
    const newSkill = {
      skill: values.skillName,
      level: values.skillRange
    };

    setNewSkills([...newSkills, newSkill]);
    resetForm();
  };

  return (
    <div className="skills">
      <button onClick={() => setIsEditOpen(!isEditOpen)}>Open Edit</button>

      {isEditOpen && (
        <Formik
          initialValues={{ skillName: '', skillRange: '' }}
          onSubmit={handleSubmit}
          validate={(values) => {
            const errors = {};

            if (!values.skillName) {
              errors.skillName = 'Skill name is a required field';
            }

            if (!values.skillRange) {
              errors.skillRange = 'Skill range is a required field';
            } else if (isNaN(values.skillRange)) {
              errors.skillRange = 'Skill range must be a number';
            } else if (values.skillRange < 10 || values.skillRange > 100) {
              errors.skillRange = 'Skill range must be a number from 10 to 100';
            }

            return errors;
          }}
        >
          <Form className="edit-form">
            <div className="form-field">
              <label htmlFor="skillName">Skill Name:</label>
              <Field type="text" placeholder="Enter skill name" id="skillName" name="skillName" />
              <ErrorMessage name="skillName" component="div" className="skill-error-message" />
            </div>

            <div className="form-field">
              <label htmlFor="skillRange">Skill Range:</label>
              <Field type="number" placeholder="Enter skill range" id="skillRange" name="skillRange" />
              <ErrorMessage name="skillRange" component="div" className="skill-error-message" />
            </div>

            <button type="submit" disabled={!isEditOpen}>
              Add Skill
            </button>
          </Form>
        </Formik>
      )}

      <div className="bar-chart">
        {skillsData.map((skill, index) => (
          <div className="bar" key={index} style={{ width: skill.level.toLowerCase() }}>
            <div className={`level ${skill.level.toLowerCase()}`}>
              <span className="skill-name">{skill.skill}</span>
            </div>
          </div>
        ))}

        {newSkills.map((skill, index) => (
          <div className="bar" key={index} style={{ width: `${skill.level}%`, backgroundColor: '#26C17E', height: '20px' }}>
            <div className={`level custom-level`}>
              <span className="skill-name">{skill.skill}</span>
            </div>
          </div>
        ))}

        <div className="breakpoints">
          {/* <div className="line"></div> */}
          <span>Beginner</span>
          <span>Proficient</span>
          <span>Expert</span>
          <span>Master</span>
        </div>
      </div>
    </div>
  );
};

export default Skills;

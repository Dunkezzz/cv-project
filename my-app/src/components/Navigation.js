import '../assets/styles/Panel.scss';
import React, { useState } from 'react';
import { scroller } from 'react-scroll';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faUser,
  faGraduationCap,
  faPen,
  faGem,
  faSuitcase,
  faPhoneFlip,
  faComment
} from "@fortawesome/free-solid-svg-icons";

const Navigation = () => {
  const [activeSection, setActiveSection] = useState('about');

  const handleSectionClick = (sectionId) => {
    scroller.scrollTo(sectionId, {
      duration: 500, // Adjust the duration as per your preference
      smooth: 'easeInOut', // Adjust the animation easing as per your preference
      offset: -50, // Adjust the scroll offset if needed
    });
    setActiveSection(sectionId);
  };

  return (
    <ul className="nav-links">
      <li>
      <button
          onClick={() => handleSectionClick('about')}
          className={activeSection === 'about' ? 'active' : ''}
        >
          <FontAwesomeIcon
            icon={faUser}
            className={activeSection === 'about' ? 'nav-icon active' : 'nav-icon'}
          />
          <span className={activeSection === 'about' ? 'active' : ''} >About Me</span>
        </button>
      </li>
      <li>
        <button
          onClick={() => handleSectionClick('education')}
          className={activeSection === 'education' ? 'active' : ''}
        >
          <FontAwesomeIcon
            icon={faGraduationCap}
            className={activeSection === 'education' ? 'nav-icon active' : 'nav-icon'}
          />
          <span className={activeSection === 'education' ? 'active' : ''}>Education</span>
        </button>
      </li>
      <li>
        <button
          onClick={() => handleSectionClick('experience')}
          className={activeSection === 'experience' ? 'active' : ''}
        >
          <FontAwesomeIcon
            icon={faPen}
            className={activeSection === 'experience' ? 'nav-icon active' : 'nav-icon'}
          />
          <span className={activeSection === 'experience' ? 'active' : ''}>Experience</span>
        </button>
      </li>
      <li>
        <button
          onClick={() => handleSectionClick('skills')}
          className={activeSection === 'skills' ? 'active' : ''}
        >
          <FontAwesomeIcon
            icon={faGem}
            className={activeSection === 'skills' ? 'nav-icon active' : 'nav-icon'}
          />
          <span  className={activeSection === 'skills' ? 'active' : ''}>Skills</span>
        </button>
      </li>
      <li>
        <button
          onClick={() => handleSectionClick('portfolio')}
          className={activeSection === 'portfolio' ? 'active' : ''}
        >
          <FontAwesomeIcon
            icon={faSuitcase}
            className={activeSection === 'portfolio' ? 'nav-icon active' : 'nav-icon'}
          />
          <span className={activeSection === 'portfolio' ? 'active' : ''}>Portfolio</span>
        </button>
      </li>
      <li>
        <button
          onClick={() => handleSectionClick('contacts')}
          className={activeSection === 'contacts' ? 'active' : ''}
        >
          <FontAwesomeIcon
            icon={faPhoneFlip}
            className={activeSection === 'contacts' ? 'nav-icon active' : 'nav-icon'}
          />
          <span className={activeSection === 'contacts' ? 'active' : ''}>Contacts</span>
        </button>
      </li>
      <li>
        <button
          onClick={() => handleSectionClick('feedback')}
          className={activeSection === 'feedback' ? 'active' : ''}
        >
          <FontAwesomeIcon
            icon={faComment}
            className={activeSection === 'feedback' ? 'nav-icon active' : 'nav-icon'}
            />
            <span className={activeSection === 'feedback' ? 'active' : ''}>Contacts</span>
          </button>
          </li>
          </ul>
            );
          };

          export default Navigation;
          

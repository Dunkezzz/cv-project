import React from 'react';
import '../assets/styles/Panel.scss'; 
import { useNavigate } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";

    
const BackButton = () => {
    const navigate = useNavigate();
  
    const handleClick = () => {
      
        navigate('/');
    };
    return (
         
         <button className='go-back-button' onClick={handleClick}><FontAwesomeIcon icon={faAngleLeft} /> <span>Go back</span></button>
         
         
    );
  };
  
  export default BackButton;
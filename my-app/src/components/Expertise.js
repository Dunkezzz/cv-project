import React from 'react';
import '../assets/styles/Expertise.scss';



const Expertise = ({ data }) => {
    return (
      <div className="expertise">
        {data.map((experience, index) => (
          <div className="experience-item" key={index}>
            <div className="experience-date">
            <div className="experience-company">{experience.info.company}</div>
              {experience.date}
            </div>
            <div className="experience-info">
              
              <div className="experience-job">{experience.info.job}</div>
              <div className="experience-description">{experience.info.description}</div>
            </div>
          </div>
        ))}
      </div>
    );
  };
  
  export default Expertise;


  
import React from 'react';
import '../assets/styles/Box.scss'; 


const Box = ({ title, content, id }) => {
    return (
      <div className="box" id={id}>
        <h2>{title}</h2>
        <div>{content}</div>
      </div>
    );
  };
  
  export default Box;
import React, { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons';
import '../assets/styles/TimeLine.scss';

const TimeLine = () => {
  const [timelineData, setTimelineData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    fetch('/api/educations')
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Network response was not ok');
        }
      })
      .then((data) => {
        setTimelineData(data);
        setIsLoading(false);
      })
      .catch((error) => {
        setIsError(true);
        setIsLoading(false);
        console.error(error);
      });


      //UNCOMMENT TO SIMULATE


  //     fetch('/api/educations/error')
  // .then((response) => {
  //   if (response.ok) {
  //     return response.json();
  //   } else {
  //     throw new Error('Network response was not ok');
  //   }
  // })
  // .then((data) => {
  //   setTimelineData(data);
  //   setIsLoading(false);
  // })
  // .catch((error) => {
  //   setIsError(true);
  //   setIsLoading(false);
  //   console.error(error);
  // });





  }, []);


  

  if (isLoading) {
    return (
      <div className="loading">
        <FontAwesomeIcon className="loading-icon" icon={faSyncAlt} spin />
      </div>
    );
  }

  if (isError) {
    return (
      <div className="error-message">
        Something went wrong; please review your server connection!
      </div>
    );
  }

  return (
    <div className="timeline">
      <div className="education-line"></div>
      {timelineData.map((event) => (
        <div key={event.id} className="timeline__event">
          <div className="timeline__date">{event.date}</div>
          <div className="timeline__content">
            <h3 className="timeline__event-title">{event.title}</h3>
            <p className="timeline__event-text">{event.description}</p>
          </div>
        </div>
      ))}
    </div>
  );
};

export default TimeLine;

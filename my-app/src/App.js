import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import Inner from './pages/Inner';

function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/inner" element={<Inner />} />
    </Routes>
  );
}

export default App;

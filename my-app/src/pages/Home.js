import React from 'react';
import { useNavigate } from 'react-router-dom';
import '../assets/styles/Home.scss';
import profileImage from '../assets/images/profile.jpg';

const Home = () => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate('/inner');
  };

  return (
    <div className="cover">
      <div className="background-image"></div>
      <div className="home-content">
        <img src={profileImage} alt="ProfilePicture" className="logo" />
        <div className="overlay"></div>
        <h1>Yerzhan Mukushev</h1>
        <h2>Visual artist. Programmer</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec ipsum sed mauris tempus malesuada ut sit amet nibh.</p>
        <button className="button" onClick={handleClick}>Learn More</button>
      </div>
    </div>
  );
};

export default Home;

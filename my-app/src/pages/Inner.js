import React, { useEffect, useState } from 'react';
import '../assets/styles/Inner.scss';
import Panel from '../components/Panel';
import Box from '../components/Box';
import TimeLine from '../components/TimeLine';
import Expertise from '../components/Expertise';
import Skills from '../components/Skills';
import Portfolio from '../components/Portfolio';
import Address from '../components/Address';
import Feedback from '../components/Feedback';
import feedbackAutor from '../assets/images/feedback.jpeg';


const Inner = () => {
  const [educations, setEducations] = useState([]);
  const [skills, setSkills] = useState([]);
  const [newSkill, setNewSkill] = useState({ name: '', range: 0 });

  useEffect(() => {
    fetch('/api/educations')
      .then((response) => response.json())
      .then((data) => setEducations(data));
  }, []);

  useEffect(() => {
    fetch('/api/skills')
      .then((response) => response.json())
      .then((data) => setSkills(data));
  }, []);

  const handleAddSkill = () => {
    fetch('/api/skills', {
      method: 'POST',
      body: JSON.stringify(newSkill),
    })
      .then((response) => response.json())
      .then((data) => {
        setSkills((prevSkills) => [...prevSkills, data]);
        setNewSkill({ name: '', range: 0 });
      });
  };

  return (
    <div className="main">
      <Panel />
      <div className="main-page">
        <Box
          title="About me"
          id="about"
          content={
            <p className="about-text">
              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel delectus, corrupti perspiciatis dolor ea sint,
              expedita consequatur dicta aspernatur, accusantium deleniti quo quaerat perferendis quidem? Veritatis quo optio
              odit beatae."
            </p>
          }
        />

        <Box
          id="education"
          title="Education"
          content={<TimeLine data={educations} />}
        />

        <Box
          title="Experience"
          id="experience"
          content={
            <Expertise
              data={[
                {
                  date: '2013-2014',
                  info: {
                    company: 'Google',
                    job: 'Front-end developer / php programmer',
                    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
                  },
                },
                {
                  date: '2012',
                  info: {
                    company: 'Twitter',
                    job: 'Web developer',
                    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
                  },
                },
              ]}
            />
          }
        />

        <Box title="Skills" id="skills" content={<Skills skills={skills} />} />
        {/* <div>
          <input
            type="text"
            placeholder="Skill Name"
            value={newSkill.name}
            onChange={(e) => setNewSkill({ ...newSkill, name: e.target.value })}
          />
          <input
            type="number"
            placeholder="Skill Range"
            value={newSkill.range}
            onChange={(e) => setNewSkill({ ...newSkill, range: parseInt(e.target.value) })}
          />
          <button onClick={handleAddSkill}>Add Skill</button>
        </div> */}

        <Box title="Portfolio" id="portfolio" content={<Portfolio />} />
        <Box title="Contacts" id="contacts" content={<Address />} />
        <Box
          title="Feedbacks"
          id="feedback"
          content={
            <Feedback
              data={[
                {
                  feedback: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
                  reporter: {
                    photoUrl: feedbackAutor,
                    name: 'John Doe',
                    citeUrl: 'https://www.citeexample.com',
                  },
                },
                {
                  feedback: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
                  reporter: {
                    photoUrl: feedbackAutor,
                    name: 'John Doe',
                    citeUrl: 'https://www.citeexample.com',
                  },
                },
              ]}
            />
          }
        />
      </div>
    </div>
  );
};

export default Inner;

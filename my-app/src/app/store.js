import { createStore } from 'redux';
import rootReducer from '../reducers';
import { configureStore } from '@reduxjs/toolkit';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import skillsReducer from '../features/skills/skillsSlice';

// const store = createStore(rootReducer);

// export default store;


const persistConfig = {
    key: 'root',
    storage
  };
  
  const persistedReducer = persistReducer(persistConfig, skillsReducer);
  
  export const store = configureStore({
    reducer: persistedReducer,
    middleware: [thunk]
  });
  
  export const persistor = persistStore(store);
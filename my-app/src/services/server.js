import { createServer, Model, Response } from 'miragejs';

export function makeServer({ environment = 'development' } = {}) {
  const server = createServer({
    environment,

    models: {
      education: Model,
      skill: Model,
    },

    seeds(server) {
      server.db.loadData({
        educations: [
          {
            id: 1,
            date: '2023-01-01',
            title: 'Education 1',
            description: 'Education 1 description',
          },
          {
            id: 2,
            date: '2023-02-01',
            title: 'Education 2',
            description: 'Education 2 description',
          },
          {
            id: 3,
            date: '2023-02-01',
            title: 'Education 2',
            description: 'Education 2 description',
          },
        ],
        skills: [
          {
            id: 1,
            name: 'Skill 1',
            range: 5,
          },
          {
            id: 2,
            name: 'Skill 2',
            range: 3,
          },
        ],
      });
    },

    routes() {
      this.namespace = 'api';

      this.get('/educations', (schema) => {
        console.log('GET /educations endpoint called');
        return schema.db.educations;
      });
    
    // Loading Error imitaion
      this.get('/educations/error', () => {
        console.log('GET /educations/error endpoint called');
        return new Response(500, {}, { error: 'Failed to fetch educational data' });
      });

      this.get('/skills', (schema) => {
        console.log('GET /skills endpoint called');
        return schema.db.skills;
      });

      this.post('/skills', (schema, request) => {
        console.log('POST /skills endpoint called');
        const attrs = JSON.parse(request.requestBody);
        const skill = schema.db.skills.create(attrs);
        return skill;
      });
    },
  });

  // Override console.log to include MirageJS prefix
  const originalConsoleLog = console.log;
  console.log = (...args) => {
    originalConsoleLog('[MirageJS]', ...args);
  };

  return server;
}

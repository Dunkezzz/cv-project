import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  skillsData: [
    { skill: 'HTML', level: 'Expert' },
    { skill: 'CSS', level: 'Proficient' },
    { skill: 'jQuery', level: 'Proficient' },
    { skill: 'PHP', level: 'Beginner' },
    { skill: 'Laravel', level: 'Proficient' }
  ],
  newSkills: []
};

const skillsSlice = createSlice({
  name: 'skills',
  initialState,
  reducers: {
    addNewSkill: (state, action) => {
      state.newSkills.push(action.payload);
    },
    resetNewSkills: (state) => {
      state.newSkills = [];
    }
  }
});

export const { addNewSkill, resetNewSkills } = skillsSlice.actions;
export default skillsSlice.reducer;
